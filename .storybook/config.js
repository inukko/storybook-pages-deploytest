import React from 'react'
import { configure, addDecorator } from '@storybook/react'
import { Center, GlobalFonts, BodyStyles, LingualaTheme } from '../src/lib'
import { ThemeProvider } from 'styled-components'
import pkg from '../package.json'
import stories from '../src/stories/index.js'

const CenterDecorator = storyFn => <Center full>{storyFn()}</Center>

addDecorator(CenterDecorator)

const GlobalStyleDecorator = storyFn => (
  <div>
    {storyFn()}
    <GlobalFonts />
    <BodyStyles />
  </div>
)

addDecorator(GlobalStyleDecorator)

// FIXME: remove ThemeProvider and replace it with ThemeProvider_
const ThemeDecorator = storyFn => (
  <ThemeProvider theme={LingualaTheme}>{storyFn()}</ThemeProvider>
)

addDecorator(ThemeDecorator)

console.info(`based on ${pkg.name}: ${pkg.version}`)

configure(() => {
  stories.forEach(story => require(`../src/stories/${story}`))
}, module)
