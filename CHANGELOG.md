# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.21.0] - 2019-10-23

### Added

- Icons: Added some missing icons

### Removed

- Icons: removed duplicated icons "verfiedUser" and "verified"

## [0.20.3] - 2019-10-16

### Added

- bundle:
  - add ES-module bundle & update rollup-configuration (terser-plugin)
- [Accordion]: add Accordion component
- a11y (accessibility):
  added storybook-add-a11y-addon to easy testing of a11y in storybook

### Changed

- fix(esm): themeGet default imports
- globalStyles:
  add border box to globalstyles
- a11y:
  make icons accessible
  fix inputshape accessibility bug
  fix Toggle accessibility

## [0.20.2] - 2019-10-02

### Added

- [README]: added sandbox link for starters

### Changed

- npm: security updates

## [0.20.1] - 2019-09-29

### Changed

- [InputShape]: center Icon inInputShape !119
- [ToggleForm]: remove circular dependency !118
- [Icon]: add rotate prop !116

## [0.20.0] - 2019-09-18

### Added

- style: add linkColor mixin export
- ToggleForm: wrapper for Toggle in InputShape

### Changed

- Link: use linkColor mixin

## [0.19.0] - 2019-09-17

### Added

- [Link]: add anchor element component

### Changed

- [Text]: is now p element, flow-direction: column, word-wrap
- npm: update to react 16.9

## [0.18.0] - 2019-09-16

### Added

- [ShapeContent]: add component for resizing in shape container (Lens & PinShape)

### Changed

- [Colorable, FormWrapper, Image, Text, Bubble, Center] Add missing PropTypes
- [Text]: add size & centered props
- [Lens, PinShape]: fix text positioning & sizing

### Removed

- [Compartment, Rotate, Shadow] Remove usages and index exports (deprecated)
- [GermanFlag] remove deprecated Icon
- [MarketIcon] remove deprecated Icon
- [TextBox] remove unused component

## [0.17.2] - 2019-09-13

### Changed

- [InputShape, MultipleLineInput, Select, Selector, SingleLineInput, Toggle] Remove circular dependency
- MultiLineInput: visibility fix
- Components: extract box shadows and border radius to theme

## [0.17.1] - 2019-09-06

### Changed

- MultipleLineInput: refactored to remove dependency on react-textarea-autosize to fix #105 (SSR builds)

### Removed

- npm: remove dependency `react-textarea-autosize`

## [0.17.0] - 2019-09-04

### Added

- Form Elements: MultipleLineInput !65

### Changed

- SingleLineInput: remove white background !96
- Select remove white background !101
- Button: remove text font !97
- Bubble: refactored shadows !98
- LingualaTheme: refactored structure !94

## [0.16.0] - 2019-08-28

### Added

- New external dependency `@styled-system/theme-get`, install it when using ui-atoms in your app

### Changed

- Theming: switched from react-studs `theme.createSelector()` to `themeGet()` of styled-system (styled-components)

- Breaking changes:

```diff
-import { ThemeProvider, theme } from '@linguala/ui-atoms'
+import { LingualaTheme } from '@linguala/ui-atoms'
+import { ThemeProvider } from 'styled-components'

-React.render(<ThemeProvider theme={theme}><App /></ThemeProvider>)
+React.render(<ThemeProvider theme={LingualaTheme}><App /></ThemeProvider>)
```

### Removed

- Dependency `react-studs` has been removed

## [0.15.0] - 2019-08-28

### Added

- Forms: Form, LabelText, InputShape, Select, FormWrapper

### Changed

- Icon: add missing propType `icon`
- Selector: inherit state from Form
- Toggle: inherit state from Form

### Removed

- npm: formik

## [0.14.0] - 2019-07-17

### Added

- Subtitle: change from h1 to p

### Changed

- Rotate: use display flex
- updated eslint-config, group and sort imports
- resolve circular imports

## [0.13.0] - 2019-07-10

### Changed

- Title component style

### Added

- Bodystyles are loaded in storybook
- Bubble: container component from marketplace

## [0.12.2] - 2019-07-09

### Changed

- Fix Colorable: theme.connect was exported as Colorable instead of the Component itself, which lead to warnings

## [0.12.1] - 2019-07-09

### Added

- npm: add formik dev dependency to test inputs with forms

### Changed

- Atom/Toggle: refactored as input checkbox element
- Atom/Toggle stories: add Toggle/withState & Toggle/Form stories
- Fix tests: freeze storybook-state addon version for testing import errors, due to a bundle problem

## [0.12.0] - 2019-07-07

### Added

- Storybook: add storybook-state addon

### Changed

- Atom toggle props changed
- available Linguala Fonts are now: 'Linguala-Title', 'Linguala-Text' and 'Linguala-Password'

## [0.11.0] - 2019-07-03

### Added

- Inputs: Add "Password Single Line Input"
- Theme: add shadows
- Icons: add Lens Icons
- Fonts: add Linguala-Password font
- Dev: add husky configuration
- scripts: cross-env-builds for all operating systems

### Changed

- remove styled-normalize dependency, use normalize as text import
- report version of ui-atoms in storybook with console.info
- testing: enable jest-styled-components, to show css instead of just classname changes
- npm: remove styled-icons dependency

## [0.10.0] - 2019-06-28

### Added

- eslint: use eslint config from @linguala/eslint-config package
- NPM: prepublishOnly to reduce chore

### Changed

- Button: Add linguala title font
- Button: Determine font family via theme fonts
- Button: lowercase text transform on Button Text
- Bundle: update rollup config to use mainFields key
- NPM: update to latest common dev dependencies
- NPM: upgrade packages
- Tests: remove @storybook/addon-info as it breaks builds

## [0.9.3] - 2019-06-12

### Changed

- fix Lens propTypes, which lead to a breaking bug on bundle load: `ReferenceError: Text is not defined`
- revert: CI: cache `node_modules` folder for faster subsequent runs, it turns out caching `node_modules` leads to even slower builds

## [0.9.2] - 2019-06-12

### Added

- New Icons
  - CleanBrain
  - ToggleArrow
  - Asteriks

### Changed

- fix Team Icon background

## [0.9.1] - 2019-06-12

### Changed

- Hotfix build: fix bundle dependencies

## [0.9.0] - 2019-06-11

### Added

- PropTypes for React Components (Björn)
- svgr run script for .svg import to React Components (Damien)
- new Icons (Tobi)
- Single Line Input Component (Damien)

### Changed

- CI: use stable node version for gitlab runner node image
- CI: cache `node_modules` folder for faster subsequent runs

## [0.8.0] - 2019-05-15

### Added

- testing improved with jest-snapshots & storyshots
- new CSS-colorable Icons & SVG-Icon-system

## [0.7.2] - 2019-04-24

### Added

- BodyStyles

### Changed

- moved GlobalFonts in a separate Files

## [0.7.1] - 2019-04-10

### Added

- Text Component with widthLimit prop

## [0.7.0] - 2019-04-10

### Changed

- Button disabled & noresize prop added, secondary prop removed
- Center component has now spacing for children
- Colorable disabled instead of idle prop

## [0.6.1] - 2019-03-27

### Changed

- fixed Button theme coloring

## [0.6.0] - 2019-02-27

### Added

- Colorable Component with secondary, danger & idle props from theme based colors

### Changed

- Lens, PinShape, ButtonShape, CircleShape color can be set with (danger, idle & secondary) props
- npm: update devDependencies & peerDependencies

## [0.5.3] - 2019-02-27

### Changed

- npm: Fixed 432 vulnerabilities (lodash)

## [0.5.2] - 2019-02-27

### Added

- Linguala blue10 color

## [0.5.1] - 2019-02-06

### Changed

- storybook: refactored into subfolders, import by `*.stories.js` name
- storybook: `addon-info` for every story

### Removed

- experimental Text component

## [0.5.0] - 2019-02-06

### Added

- Linguala Logo asset

### Changed

- components: Logo is now only the Logo image without name

## [0.4.1] - 2018-11-28

### Changed

- Update dependencies
- Testing: ava, use modules

## [0.4.0] - 2018-10-31

### Added

- Atom German Flag Shape

### Changed

- set default browser font-size on body to 16px and use em for all others
- add project formatter config for vscode

## [0.3.6] - 2018-09-12

### Changed

- upgrade to styled-components@4
- renamed injectFonts to GlobalFonts (migration), use as Component instead of function

## [0.3.5] - 2018-08-29

### Changed

- dropped @linguala/react-dependencies
- upgrade to babel 7 & subsequent configuration update
- workaround for img src quotes

## [0.3.4] - 2018-08-15

### Added

- feature(Compartment): add new Layout component & stories

### Changed

- refactor(Center): move height directive out
- chore(npm): update dependencies

## [0.3.3] - 2018-08-15

### Added

- Title component

### Changed

- fonts: set font on body globally

## [0.3.2] - 2018-07-25

### Added

- API: use injectFonts() to make fonts globally available in your app

### Changed

- bundle: bundle assets only as data-uri until #44 is fixed

### Removed

- bundle: font asset is not in ui-atoms.js included, not as a separate file

## [0.3.1] - 2018-07-11

### Changed

- bundle: don't bundle styled-components
- bundle: only upload dist bundle for npm

## [0.3.0] - 2018-05-30

### Changed

- Refactoring for theming of a few components
  - CircleShape
  - PinShape
  - Lens
  - ButtonShape
- Minor Fixes to styling
- add embedding functionality for CircleShape
- unit(), get defaults from theme file
- ButtonShape: border-radius size calculation
- Button is now a button element, not input anymore
- Icon, width & size prop added

### Added

- Components
  - FullWidthButton
  - Center container component
  - TextBox for FullWidthButton

## [0.2.4] - 2018-05-16

### Changed

- Fix build of dist/ui-atoms

## [0.2.3] - 2018-05-16

### Changed

- Components:
  - fix Button border overlapping button text
  - ButtonShape noborder attribute added

## [0.2.2] - 2018-05-16

### Changed

- Update dependency to @linguala/react-dependencies, to get rid of react-codemod

## [0.2.1] - 2018-05-16

### Fix

- Updated bundle configuration, .nvmrc to latest node 8 version

## [0.2.0] - 2018-05-16

### Added

- Theming
- Colors: Linguala Colors
- Components:
  - Atom [Shape](https://app.zeplin.io/project/59f0606cb4c8ae0eea80cba9?seid=5abbb2204ff6dff0407445d8)
    - ButtonShape full / border variants
    - CircleShape full / border variants
    - PinShape full / border variants

### Changed

- Components:
  - Rename Pin -> Lens
  - Rename PinImage -> LensImage

## [0.1.0] - 2017-12-18

### Added

- Components:
  - Atom [Call to Action Button](https://app.zeplin.io/project/59f0606cb4c8ae0eea80cba9/screen/5a2aeacbb945d5e25ee321b1)

## [0.0.3] - 2017-12-06

### Removed

- Assets: LingualaIcon.svg, Pin.svg, defaultIcon.svg, logo-pt-sans.svg

## [0.0.2] - 2017-11-29

### Added

- Components:
  - Pin [Atom Pin Small](https://app.zeplin.io/project/59f0606cb4c8ae0eea80cba9/screen/59f09b0fb4c8ae0eea83ac29)
  - Logo [Molecule Linguala Logo Small](https://app.zeplin.io/project/59f0606cb4c8ae0eea80cba9/screen/59f1cb10b02f5d8e29d71db0)
- Containers:
  - Rotate
  - Shadow
  - Theme
- styles:
  - Border
  - Center

### Changed

### Removed
