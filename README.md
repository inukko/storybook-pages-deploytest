# ui-atoms

Component library for linguala atoms

[![pipeline status](https://gitlab.linguala.com/linguala/ui-atoms/badges/master/pipeline.svg)](https://gitlab.linguala.com/linguala/ui-atoms/commits/master)

## Play around in the sandbox

https://codesandbox.io/s/competent-frost-x577r

## Public Storybook

http://linguala.lng.li/ui-atoms

## NPM

https://www.npmjs.com/package/@linguala/ui-atoms

## Developing

1. Clone the repo with `git clone ssh://git@gitlab.linguala.com:1022/linguala/ui-atoms.git`
1. Install dependencies with `npm install`
1. Run `npm run dev` to start tests in the background & storybook.


## Designs

Component definitions should be at [https://gitlab.linguala.com/linguala/design](https://gitlab.linguala.com/linguala/design/tree/master/render).

[Zeplin](https://app.zeplin.io/project/59f0606cb4c8ae0eea80cba9)

