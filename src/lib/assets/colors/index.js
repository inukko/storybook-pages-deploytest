const green = '#1CB569'
const blue = '#4A90E2'
const grey = '#9B9B9B'
const red = '#BB2D00'
const green10 = '#e8f7f0'
const blue10 = '#ecf3fc'

export { green, blue, grey, red, blue10, green10 }

export default {
  green,
  blue,
  grey,
  red,
  blue10,
  green10,
}
