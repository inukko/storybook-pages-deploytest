import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { themeGet } from '@styled-system/theme-get'

const StyledContainer = styled.div`
  width: 100%;
`

const StyledDetails = styled.details`
  width: 100%;
`

const StyledSummary = styled.summary`
  color: transparent;
  width: 100%;
  cursor: pointer;
`

const StyledHeaderContainer = styled.div`
  color: ${themeGet('colors.primary')};
  width: 100%;
  margin-top: -1em;
  :hover {
    color: ${themeGet('colors.secondary')};
  }
`

const Header = props => {
  if (props.header instanceof Function) {
    return props.header()
  } else if (props.header) {
    return <p>{props.header}</p>
  }
  return <p>Missing or Invalid Header</p>
}

const Body = props => {
  if (props.body instanceof Function) {
    return props.body()
  } else if (props.body) {
    return <p>{props.body}</p>
  }
  return <p>Missing or Invalid Body</p>
}

const Accordion = props => (
  <>
    <StyledContainer>
      <StyledDetails>
        <StyledSummary>
          <StyledHeaderContainer>
            <Header {...props} />
          </StyledHeaderContainer>
        </StyledSummary>
        <Body {...props} />
      </StyledDetails>
    </StyledContainer>
  </>
)

Accordion.propTypes = {
  header: PropTypes.oneOfType([PropTypes.string, PropTypes.func]).isRequired,
  body: PropTypes.oneOfType([PropTypes.string, PropTypes.func]).isRequired,
}

export default Accordion
