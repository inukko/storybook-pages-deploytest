import styled from 'styled-components'
import PropTypes from 'prop-types'
import themeGet from '@styled-system/theme-get'

const colorType = ({ secondary, danger, disabled }) =>
  danger ? 'danger' : disabled ? 'idle' : secondary ? 'secondary' : 'primary'

const Colorable = styled.button`
  color: white;
  background-color: ${({ shapeColor, secondary, danger, disabled }) =>
    shapeColor ||
    themeGet(`colors.${colorType({ secondary, danger, disabled })}`)};

  transition: background-color 0.1s linear;
  :active,
  :hover {
    background-color: ${({ secondary, disabled }) =>
      disabled
        ? themeGet('colors.idle')
        : !secondary
        ? themeGet('colors.secondary')
        : themeGet('colors.primary')};
  }
`

Colorable.propTypes = {
  shapeColor: PropTypes.string,
  secondary: PropTypes.bool,
  danger: PropTypes.bool,
  disabled: PropTypes.bool,
}

export default Colorable
