import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'

import FormContext from './FormContext.js'

function Form(props) {
  const [state, setState] = useState(props.initFormState)

  useEffect(() => {
    if (props.handleFormChange) {
      props.handleFormChange(state)
    }
    // console.info('STATE', JSON.stringify(state))
  })
  const setFormState = newState => {
    setState(prevState => {
      return { ...prevState, ...newState }
    })
  }

  const handleFormSubmit = event => {
    event.preventDefault()
    if (props.handleFormSubmit) {
      props.handleFormSubmit(state)
    }
  }

  return (
    <React.Fragment>
      <form onSubmit={handleFormSubmit}>
        <FormContext.Provider value={[state, setFormState]}>
          {props.children}
        </FormContext.Provider>
      </form>
    </React.Fragment>
  )
}

Form.propTypes = {
  initFormState: PropTypes.object,
  handleFormSubmit: PropTypes.func,
  handleFormChange: PropTypes.func,
}

export default Form
