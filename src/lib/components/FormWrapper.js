import React, { useContext } from 'react'
import PropTypes from 'prop-types'

import FormContext from '../components/FormContext'

function FormWrapper(props) {
  const [getFormState, setFormState] = useContext(FormContext)
  const name = props.name
  const child = props.render(name)
  return React.cloneElement(child, {
    ...props,
    ...getFormState[name],
    setFormState: formChildState => {
      setFormState({
        [name]: { ...formChildState },
      })
    },
  })
}

FormWrapper.propTypes = {
  name: PropTypes.string,
}

export default FormWrapper
