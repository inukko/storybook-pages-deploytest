import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import themeGet from '@styled-system/theme-get'

import { unit } from '../utils'

const StyledIcon = styled.div`
${props => (props.rotate ? `transform: rotate(${props.rotate}deg);` : null)}
svg [fill="${themeGet('colors.primary')}"] {
  fill: ${props => props.colorPrimary};
}
svg [stroke="${themeGet('colors.primary')}"] {
  stroke: ${props => props.colorPrimary};
}

svg :hover [fill="${themeGet('colors.primary')}"] {
  fill: ${props =>
    props.colorPrimaryHover === 'true' || props.colorPrimaryHover === true
      ? themeGet('colors.secondary')
      : props.colorPrimaryHover};
}
svg :hover [stroke="${themeGet('colors.primary')}"] {
  stroke: ${props =>
    props.colorPrimaryHover === 'true' || props.colorPrimaryHover === true
      ? themeGet('colors.secondary')
      : props.colorPrimaryHover};
}

svg [fill="${themeGet('colors.secondary')}"] {
  fill: ${props => props.colorSecondary};
}
svg [stroke="${themeGet('colors.secondary')}"] {
  stroke: ${props => props.colorSecondary};
}

svg :hover [fill="${themeGet('colors.secondary')}"] {
  fill: ${props =>
    props.colorSecondaryHover === 'true' || props.colorSecondaryHover === true
      ? themeGet('colors.primary')
      : props.colorSecondaryHover};
}

svg :hover [stroke="${themeGet('colors.secondary')}"] {
  stroke: ${props =>
    props.colorSecondaryHover === 'true' || props.colorSecondaryHover === true
      ? themeGet('colors.primary')
      : props.colorSecondaryHover};
}
`

const Icon = props => {
  const width = unit(props.size || 4)
  const iconContent = props.icon ? (
    <props.icon width={width} />
  ) : (
    <div>{props.alt || 'Empty Icon'}</div>
  )
  return (
    <StyledIcon role="img" aria-label={props.alt} {...props}>
      {iconContent}
    </StyledIcon>
  )
}

Icon.propTypes = {
  icon: PropTypes.func.isRequired,
  alt: PropTypes.string.isRequired,
  colorPrimary: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.func,
  ]),
  colorPrimaryHover: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.func,
  ]),
  colorSecondary: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.func,
  ]),
  colorSecondaryHover: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.func,
  ]),
}

export default Icon
