import React from 'react'

const SvgCheckmark = props => (
  <svg viewBox="0 0 16 14" {...props}>
    <path
      fill="#4A90E2"
      d="M1.5 8.5L0 10l4 4L15.591 2.402 14.064.976 4 11z"
      fillRule="evenodd"
    />
  </svg>
)

export default SvgCheckmark
