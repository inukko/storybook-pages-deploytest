import React from 'react'

const SvgCommunication = props => (
  <svg viewBox="0 0 42 42" {...props}>
    <defs>
      <path id="Communication_svg__a" d="M0 0h40v40H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <g transform="translate(2)">
        <mask id="Communication_svg__b" fill="#fff">
          <use xlinkHref="#Communication_svg__a" />
        </mask>
        <path
          d="M20 39.2C9.396 39.2.8 30.604.8 20S9.396.8 20 .8 39.2 9.396 39.2 20 30.604 39.2 20 39.2z"
          fill="#1CB569"
          fillRule="nonzero"
          mask="url(#Communication_svg__b)"
        />
      </g>
      <g fill="#FFF">
        <path d="M35.329 28.591c1.31-6.645.423-11.513-2.664-14.603-3.385-3.386-8.948-3.484-12.217-.214-3.269 3.269-3.17 8.867.215 12.253 3.334 3 8.222 3.854 14.666 2.564z" />
        <path
          d="M10.429 26.46c-2.358-6.365-2.275-11.569.334-15.587 3.055-4.205 9.037-5.26 13.117-2.295 4.08 2.964 4.92 8.943 1.865 13.148-2.738 3.765-7.646 5.487-14.641 5.223l-.501-.019-.174-.47z"
          stroke="#1CB569"
          strokeWidth={1.5}
        />
      </g>
    </g>
  </svg>
)

export default SvgCommunication
