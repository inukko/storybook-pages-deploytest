import React from 'react'

const SvgDollar = props => (
  <svg viewBox="0 0 25 25" fill="none" {...props}>
    <mask
      id="Dollar_svg__a"
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={25}
      height={25}
    >
      <path fill="#fff" d="M0 0h25v25H0z" />
    </mask>
    <g mask="url(#Dollar_svg__a)">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M24.5 12.5c0 6.627-5.373 12-12 12s-12-5.373-12-12 5.373-12 12-12 12 5.373 12 12z"
        fill="#1CB569"
      />
    </g>
    <path
      d="M13.48 10.9c-2.27-.59-3-1.2-3-2.15 0-1.09 1.01-1.85 2.7-1.85 1.78 0 2.44.85 2.5 2.1h2.21c-.07-1.72-1.12-3.3-3.21-3.81V3h-3v2.16c-1.94.42-3.5 1.68-3.5 3.61 0 2.31 1.91 3.46 4.7 4.13 2.5.6 3 1.48 3 2.41 0 .69-.49 1.79-2.7 1.79-2.06 0-2.87-.92-2.98-2.1H8c.12 2.19 1.76 3.42 3.68 3.83V21h3v-2.15c1.95-.37 3.5-1.5 3.5-3.55 0-2.84-2.43-3.81-4.7-4.4z"
      fill="#fff"
    />
  </svg>
)

export default SvgDollar
