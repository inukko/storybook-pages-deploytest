import React from 'react'

const SvgErrorIcon = props => (
  <svg viewBox="0 0 25 25" {...props}>
    <g fill="none" fillRule="evenodd">
      <path
        d="M12.5.5C5.876.5.5 5.876.5 12.5s5.376 12 12 12 12-5.376 12-12-5.376-12-12-12z"
        fill="#BB2D00"
        stroke="#FFF"
      />
      <path
        d="M14.2 20H11v-3.2h3.2V20zm0-6.4H11V4h3.2v9.6z"
        fill="#FFF"
        fillRule="nonzero"
      />
    </g>
  </svg>
)

export default SvgErrorIcon
