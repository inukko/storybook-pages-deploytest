import React from 'react'

const SvgEuro = props => (
  <svg viewBox="0 0 25 25" {...props}>
    <defs>
      <path id="Euro_svg__a" d="M0 0h25v25H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <mask id="Euro_svg__b" fill="#fff">
        <use xlinkHref="#Euro_svg__a" />
      </mask>
      <path
        d="M12.5 24.5c-6.627 0-12-5.373-12-12s5.373-12 12-12 12 5.373 12 12-5.373 12-12 12z"
        fill="#1CB569"
        fillRule="nonzero"
        mask="url(#Euro_svg__b)"
      />
      <path
        d="M14 19.5A6.48 6.48 0 0 1 8.24 16H14v-2H7.58c-.05-.33-.08-.66-.08-1 0-.34.03-.67.08-1H14v-2H8.24A6.491 6.491 0 0 1 14 6.5c1.61 0 3.09.59 4.23 1.57L20 6.3A8.955 8.955 0 0 0 14 4c-3.92 0-7.24 2.51-8.48 6H2v2h3.06a8.262 8.262 0 0 0 0 2H2v2h3.52c1.24 3.49 4.56 6 8.48 6 2.31 0 4.41-.87 6-2.3l-1.78-1.77c-1.13.98-2.6 1.57-4.22 1.57z"
        fill="#FFF"
        fillRule="nonzero"
      />
    </g>
  </svg>
)

export default SvgEuro
