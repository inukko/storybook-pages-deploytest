import React from 'react'

const SvgEye = props => (
  <svg viewBox="0 0 25 25" {...props}>
    <defs>
      <path id="Eye_svg__a" d="M0 0h25v25H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <mask id="Eye_svg__b" fill="#fff">
        <use xlinkHref="#Eye_svg__a" />
      </mask>
      <path
        d="M12.5 24.5c-6.627 0-12-5.373-12-12s5.373-12 12-12 12 5.373 12 12-5.373 12-12 12z"
        fill="#1CB569"
        fillRule="nonzero"
        mask="url(#Eye_svg__b)"
      />
      <path
        d="M13 6c-4.09 0-7.585 2.545-9 6.136 1.415 3.592 4.91 6.137 9 6.137s7.585-2.545 9-6.137C20.585 8.545 17.09 6 13 6zm0 10.227a4.092 4.092 0 0 1-4.09-4.09A4.092 4.092 0 0 1 13 8.044a4.092 4.092 0 0 1 4.09 4.091A4.092 4.092 0 0 1 13 16.227zm0-6.545a2.451 2.451 0 0 0-2.455 2.454A2.451 2.451 0 0 0 13 14.591a2.451 2.451 0 0 0 2.455-2.455A2.451 2.451 0 0 0 13 9.682z"
        fill="#FFF"
        fillRule="nonzero"
      />
    </g>
  </svg>
)

export default SvgEye
