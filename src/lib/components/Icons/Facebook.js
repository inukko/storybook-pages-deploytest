import React from 'react'

const SvgFacebook = props => (
  <svg viewBox="0 0 25 25" {...props}>
    <defs>
      <path id="Facebook_svg__a" d="M0 0h25v25H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <g>
        <mask id="Facebook_svg__b" fill="#fff">
          <use xlinkHref="#Facebook_svg__a" />
        </mask>
        <path
          d="M12.5 24.5c-6.627 0-12-5.373-12-12s5.373-12 12-12 12 5.373 12 12-5.373 12-12 12z"
          fill="#1CB569"
          fillRule="nonzero"
          mask="url(#Facebook_svg__b)"
        />
      </g>
      <path
        d="M15.637 12.486h-2.051V20h-3.108v-7.514H9V9.845h1.478v-1.71c0-1.221.58-3.135 3.135-3.135l2.302.01v2.563h-1.67c-.274 0-.66.137-.66.72v1.554h2.323l-.271 2.639z"
        fill="#FFF"
        fillRule="nonzero"
      />
    </g>
  </svg>
)

export default SvgFacebook
