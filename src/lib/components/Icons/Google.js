import React from 'react'

const SvgGoogle = props => (
  <svg viewBox="0 0 26 26" fill="none" {...props}>
    <mask
      id="Google_svg__a"
      maskUnits="userSpaceOnUse"
      x={1}
      y={1}
      width={25}
      height={25}
    >
      <path fill="#fff" d="M1 1h25v25H1z" />
    </mask>
    <g mask="url(#Google_svg__a)">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M25.5 13.5c0 6.627-5.373 12-12 12s-12-5.373-12-12 5.373-12 12-12 12 5.373 12 12z"
        fill="#1CB569"
      />
    </g>
    <path
      d="M19.862 11.688c.09.468.138.958.138 1.47C20 17.156 17.268 20 13.143 20 9.197 20 6 16.867 6 13s3.197-7 7.143-7c1.929 0 3.54.695 4.777 1.824l-2.013 1.973c-.75-.704-1.7-1.064-2.764-1.064-2.357 0-4.272 1.951-4.272 4.261s1.916 4.266 4.272 4.266c2.14 0 3.594-1.198 3.894-2.843h-3.894v-2.73l6.719.001z"
      fill="#fff"
    />
  </svg>
)

export default SvgGoogle
