import React from 'react'

const SvgHeart = props => (
  <svg viewBox="0 0 42 43" {...props}>
    <defs>
      <path id="Heart_svg__a" d="M0 0h43v43H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <g transform="translate(-.435 -.07)">
        <mask id="Heart_svg__b" fill="#fff">
          <use xlinkHref="#Heart_svg__a" />
        </mask>
        <path
          d="M21.5 42.14C10.1 42.14.86 32.9.86 21.5S10.1.86 21.5.86 42.14 10.1 42.14 21.5 32.9 42.14 21.5 42.14z"
          fill="#1CB569"
          fillRule="nonzero"
          mask="url(#Heart_svg__b)"
        />
      </g>
      <path
        d="M21.15 33.661c-.046-.056-.078-.102-.118-.142-3.33-3.316-6.66-6.632-9.992-9.945-.978-.972-1.727-2.084-2.128-3.408-.622-2.057-.419-4.033.706-5.87 1.292-2.11 3.207-3.313 5.679-3.534 2.185-.195 4.107.482 5.718 1.986l.143.132c.232-.198.456-.403.694-.589 1.346-1.047 2.876-1.558 4.583-1.56a7.36 7.36 0 0 1 6.89 4.814c.616 1.682.57 3.368-.045 5.043-.415 1.125-1.084 2.087-1.93 2.93-3.357 3.344-6.717 6.686-10.075 10.03l-.124.113z"
        fill="#FFF"
        fillRule="nonzero"
      />
    </g>
  </svg>
)

export default SvgHeart
