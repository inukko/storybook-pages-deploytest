import React from 'react'

const SvgInstagram = props => (
  <svg viewBox="0 0 25 25" fill="none" {...props}>
    <mask
      id="Instagram_svg__a"
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={25}
      height={25}
    >
      <path fill="#fff" d="M0 0h25v25H0z" />
    </mask>
    <g mask="url(#Instagram_svg__a)">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M24.5 12.5c0 6.627-5.373 12-12 12s-12-5.373-12-12 5.373-12 12-12 12 5.373 12 12z"
        fill="#1CB569"
      />
    </g>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M8.201 5h9.598C19.014 5 20 5.986 20 7.203v9.598a2.202 2.202 0 0 1-2.201 2.201H8.201A2.202 2.202 0 0 1 6 16.802V7.202C6 5.986 6.986 5 8.201 5zm9.869 1.615l-1.76.004a.303.303 0 0 0-.302.307l.007 1.765c0 .167.135.302.304.302l1.757-.006a.303.303 0 0 0 .304-.304V6.918a.303.303 0 0 0-.306-.303h-.004zm-7.216 4.183A2.46 2.46 0 0 1 13 9.537a2.461 2.461 0 0 1 2.147 1.264c.2.354.319.764.319 1.2a2.466 2.466 0 0 1-4.93 0c0-.436.117-.846.32-1.203zm6.44 6.841c.74 0 1.343-.602 1.343-1.342v-5.732h-2.09a3.83 3.83 0 0 1 .281 1.436 3.833 3.833 0 0 1-3.829 3.829 3.832 3.832 0 0 1-3.68-4.871 3.65 3.65 0 0 1 .135-.394h-2.09v5.732c0 .74.601 1.342 1.341 1.342h8.59z"
      fill="#fff"
    />
  </svg>
)

export default SvgInstagram
