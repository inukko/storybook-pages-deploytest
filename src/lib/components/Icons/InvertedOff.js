import React from 'react'

const SvgInvertedOff = props => (
  <svg viewBox="0 0 17 17" {...props}>
    <path
      fill="#FFF"
      fillRule="evenodd"
      d="M8.395 16.79a8.395 8.395 0 1 1 0-16.79 8.395 8.395 0 0 1 0 16.79zm2.544-5.411a.51.51 0 0 0 0-.72L8.79 8.511l2.18-2.181a.515.515 0 0 0 0-.726.514.514 0 0 0-.723 0l-2.18 2.182-2.148-2.151a.507.507 0 1 0-.717.72L7.35 8.502 5.187 10.67c-.2.198-.2.522 0 .725a.515.515 0 0 0 .723 0l2.163-2.166 2.148 2.151a.508.508 0 0 0 .718 0z"
    />
  </svg>
)

export default SvgInvertedOff
