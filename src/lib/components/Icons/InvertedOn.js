import React from 'react'

const SvgInvertedOn = props => (
  <svg viewBox="0 0 17 17" {...props}>
    <path
      fill="#FFF"
      fillRule="evenodd"
      d="M8.395 16.792A8.396 8.396 0 0 1 0 8.396a8.396 8.396 0 1 1 16.79 0 8.396 8.396 0 0 1-8.395 8.396zm3.083-10.693a.417.417 0 0 0-.164-.583.46.46 0 0 0-.612.154l-2.866 4.757L6.232 9.01a.463.463 0 0 0-.633-.017.415.415 0 0 0-.021.605l2.05 1.812c.168.171.452.18.632.017.055-.047 3.218-5.328 3.218-5.328z"
    />
  </svg>
)

export default SvgInvertedOn
