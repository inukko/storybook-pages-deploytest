import React from 'react'

const SvgMembership = props => (
  <svg viewBox="0 0 25 25" {...props}>
    <defs>
      <path id="Membership_svg__a" d="M0 0h25v25H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <mask id="Membership_svg__b" fill="#fff">
        <use xlinkHref="#Membership_svg__a" />
      </mask>
      <path
        d="M12.5 24.5c-6.627 0-12-5.373-12-12s5.373-12 12-12 12 5.373 12 12-5.373 12-12 12z"
        fill="#1CB569"
        fillRule="nonzero"
        mask="url(#Membership_svg__b)"
      />
      <path
        d="M18.9 5.5H6.1c-.888 0-1.6.712-1.6 1.6v8.8c0 .888.712 1.6 1.6 1.6h3.2v4l3.2-1.6 3.2 1.6v-4h3.2c.888 0 1.6-.712 1.6-1.6V7.1c0-.888-.712-1.6-1.6-1.6zm0 10.4H6.1v-1.6h12.8v1.6zm0-4H6.1V7.1h12.8v4.8z"
        fill="#FFF"
        fillRule="nonzero"
      />
    </g>
  </svg>
)

export default SvgMembership
