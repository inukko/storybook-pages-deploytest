import React from 'react'

const SvgPhone = props => (
  <svg viewBox="0 0 25 25" {...props}>
    <g fill="none" fillRule="evenodd">
      <circle fill="#1CB569" cx={12.5} cy={12.5} r={12.5} />
      <path
        d="M8.017 11.492a12.624 12.624 0 0 0 5.491 5.491l1.834-1.833a.829.829 0 0 1 .85-.2 9.506 9.506 0 0 0 2.975.475c.458 0 .833.375.833.833v2.909a.836.836 0 0 1-.833.833C11.342 20 5 13.658 5 5.833 5 5.375 5.375 5 5.833 5H8.75c.458 0 .833.375.833.833 0 1.042.167 2.042.475 2.975a.836.836 0 0 1-.208.85l-1.833 1.834z"
        fill="#FFF"
        fillRule="nonzero"
      />
    </g>
  </svg>
)

export default SvgPhone
