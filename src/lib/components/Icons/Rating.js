import React from 'react'

const SvgRating = props => (
  <svg viewBox="0 0 25 25" {...props}>
    <defs>
      <path id="Rating_svg__a" d="M0 0h25v25H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <mask id="Rating_svg__b" fill="#fff">
        <use xlinkHref="#Rating_svg__a" />
      </mask>
      <path
        d="M12.5 24.5c-6.627 0-12-5.373-12-12s5.373-12 12-12 12 5.373 12 12-5.373 12-12 12z"
        fill="#1CB569"
        fillRule="nonzero"
        mask="url(#Rating_svg__b)"
      />
      <path
        fill="#FFF"
        fillRule="nonzero"
        d="M13 17.466L18.562 21l-1.476-6.66L22 9.859l-6.471-.578L13 3l-2.529 6.281L4 9.86l4.914 4.481L7.438 21z"
      />
    </g>
  </svg>
)

export default SvgRating
