import React from 'react'

const SvgRegister = props => (
  <svg viewBox="0 0 40 40" {...props}>
    <defs>
      <path id="Register_svg__a" d="M0 0h40v40H0z" />
      <path id="Register_svg__c" d="M0 0h40v40H0z" />
      <path id="Register_svg__e" d="M0 0h17v24H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <mask id="Register_svg__b" fill="#fff">
        <use xlinkHref="#Register_svg__a" />
      </mask>
      <g mask="url(#Register_svg__b)">
        <mask id="Register_svg__d" fill="#fff">
          <use xlinkHref="#Register_svg__c" />
        </mask>
        <path
          d="M20 39.2C9.396 39.2.8 30.604.8 20S9.396.8 20 .8 39.2 9.396 39.2 20 30.604 39.2 20 39.2z"
          fill="#1CB569"
          fillRule="nonzero"
          mask="url(#Register_svg__d)"
        />
        <g transform="translate(11.2 8)">
          <mask id="Register_svg__f" fill="#fff">
            <use xlinkHref="#Register_svg__e" />
          </mask>
          <g mask="url(#Register_svg__f)" fill="#FFF">
            <path
              d="M4.66 8.966c-.938-1.028-1.424-2.41-1.389-3.828.035-1.453.625-2.764 1.666-3.72A5.11 5.11 0 0 1 8.477 0c1.458 0 2.881.602 3.853 1.701 1.978 2.162 1.84 5.564-.278 7.548a5.11 5.11 0 0 1-3.54 1.418c-1.458 0-2.881-.603-3.853-1.701zM10.157 12C13.935 12 17 15.373 17 19.53V24H0v-4.47C0 15.372 3.065 12 6.843 12h3.315z"
              fillRule="nonzero"
            />
          </g>
        </g>
        <path
          d="M20.174 27.51l.161-.772c.066-.309.118-.548.173-.78.058-.249.189-.439.366-.616 2.61-2.618 5.313-5.325 8.729-8.743.546-.547 1.26-.7 1.941-.394.667.3 1.07.991.945 1.723-.061.36-.236.724-.489.98a3185.27 3185.27 0 0 1-8.81 8.828 1.274 1.274 0 0 1-.605.34c-.207.046-.42.093-.682.148l-.842.175-1.126.236.239-1.126z"
          stroke="#1CB569"
          strokeWidth={1.5}
          fill="#FFF"
        />
      </g>
    </g>
  </svg>
)

export default SvgRegister
