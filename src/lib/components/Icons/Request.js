import React from 'react'

const SvgRequest = props => (
  <svg viewBox="0 0 25 25" fill="none" {...props}>
    <mask
      id="Request_svg__a"
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={25}
      height={25}
    >
      <path fill="#fff" d="M0 0h25v25H0z" />
    </mask>
    <g mask="url(#Request_svg__a)">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M24.5 12.5c0 6.627-5.373 12-12 12s-12-5.373-12-12 5.373-12 12-12 12 5.373 12 12z"
        fill="#1CB569"
      />
    </g>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M4.25 14.912V18h3.088L17.2 8.162a.619.619 0 0 0 0-.887l-2.212-2.213a.619.619 0 0 0-.888 0l-9.85 9.85zM9.875 18h9.375v-2.5h-6.875l-2.5 2.5z"
      fill="#fff"
    />
  </svg>
)

export default SvgRequest
