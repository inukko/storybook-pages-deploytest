import React from 'react'

const SvgRssFeed = props => (
  <svg viewBox="0 0 25 25" {...props}>
    <defs>
      <path id="RssFeed_svg__a" d="M0 0h25v25H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <mask id="RssFeed_svg__b" fill="#fff">
        <use xlinkHref="#RssFeed_svg__a" />
      </mask>
      <path
        d="M12.5 24.5c-6.627 0-12-5.373-12-12s5.373-12 12-12 12 5.373 12 12-5.373 12-12 12z"
        fill="#1CB569"
        fillRule="nonzero"
        mask="url(#RssFeed_svg__b)"
      />
      <g transform="rotate(-45 15.328 3.672)" fill="#FFF" fillRule="nonzero">
        <circle cx={2.337} cy={12.663} r={2.044} />
        <path d="M.293.12v2.653c6.59 0 11.934 5.343 11.934 11.934h2.653C14.88 6.654 8.346.12.293.12zm0 5.306v2.653a6.634 6.634 0 0 1 6.628 6.628h2.653A9.278 9.278 0 0 0 .293 5.426z" />
      </g>
    </g>
  </svg>
)

export default SvgRssFeed
