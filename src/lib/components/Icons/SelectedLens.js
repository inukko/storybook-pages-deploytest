import React from 'react'

const SvgSelectedLens = props => (
  <svg viewBox="0 0 35 42" {...props}>
    <defs>
      <path id="SelectedLens_svg__a" d="M0 0h60v60H0z" />
    </defs>
    <g transform="translate(-13 -13)" fill="none" fillRule="evenodd">
      <mask id="SelectedLens_svg__b" fill="#fff">
        <use xlinkHref="#SelectedLens_svg__a" />
      </mask>
      <g mask="url(#SelectedLens_svg__b)">
        <circle cx={30} cy={15} r={15} />
        <circle cx={45} cy={30} r={15} />
        <circle cx={15} cy={30} r={15} />
        <circle cx={30} cy={45} r={15} />
        <path
          d="M30.566 53.512c.607-.367 1.33-.84 2.105-1.385a45.165 45.165 0 0 0 6.282-5.33c2.492-2.556 4.483-5.277 5.812-8.12 1.55-3.317 2.154-6.703 1.675-10.152C45.278 20.159 38.604 14.2 30.354 14.2c-8.225 0-14.802 5.927-15.975 14.326-.535 3.836.139 7.491 1.854 10.98 1.448 2.944 3.596 5.687 6.269 8.21a43.663 43.663 0 0 0 5.806 4.591 46.18 46.18 0 0 0 2.085 1.309l.173-.104zM47.63 28.36c2.186 15.746-17.274 26.774-17.274 26.639 0-.135-19.476-10.076-17.164-26.64C14.436 19.438 21.463 13 30.354 13c8.89 0 16.036 6.443 17.275 15.36z"
          fill="#1CB569"
          fillRule="nonzero"
        />
      </g>
    </g>
  </svg>
)

export default SvgSelectedLens
