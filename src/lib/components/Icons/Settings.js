import React from 'react'

const SvgSettings = props => (
  <svg viewBox="0 0 25 25" {...props}>
    <defs>
      <path id="Settings_svg__a" d="M0 0h25v25H0z" />
      <path id="Settings_svg__c" d="M0 0h25v25H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <mask id="Settings_svg__b" fill="#fff">
        <use xlinkHref="#Settings_svg__a" />
      </mask>
      <g mask="url(#Settings_svg__b)">
        <mask id="Settings_svg__d" fill="#fff">
          <use xlinkHref="#Settings_svg__c" />
        </mask>
        <path
          d="M12.5 24.5c-6.627 0-12-5.373-12-12s5.373-12 12-12 12 5.373 12 12-5.373 12-12 12z"
          fill="#1CB569"
          fillRule="nonzero"
          mask="url(#Settings_svg__d)"
        />
      </g>
      <path
        d="M18.3 13.15c.033-.267.058-.533.058-.817 0-.283-.025-.55-.059-.816l1.759-1.375a.42.42 0 0 0 .1-.534L18.49 6.725a.419.419 0 0 0-.508-.183l-2.075.833a6.088 6.088 0 0 0-1.409-.817l-.316-2.208a.406.406 0 0 0-.409-.35h-3.333a.406.406 0 0 0-.408.35l-.317 2.208a6.402 6.402 0 0 0-1.408.817l-2.075-.833a.406.406 0 0 0-.509.183L4.058 9.608a.41.41 0 0 0 .1.534l1.758 1.375a6.609 6.609 0 0 0-.058.816c0 .275.025.55.058.817l-1.758 1.375a.42.42 0 0 0-.1.533l1.666 2.884c.1.183.325.25.509.183l2.075-.833c.433.333.9.608 1.408.816l.317 2.209c.025.2.2.35.408.35h3.333c.209 0 .384-.15.409-.35l.316-2.209a6.402 6.402 0 0 0 1.409-.816l2.075.833c.191.075.408 0 .508-.183l1.667-2.884a.42.42 0 0 0-.1-.533l-1.759-1.375zm-6.192 2.1a2.92 2.92 0 0 1-2.917-2.917 2.92 2.92 0 0 1 2.917-2.916 2.92 2.92 0 0 1 2.916 2.916 2.92 2.92 0 0 1-2.916 2.917z"
        fill="#FFF"
        fillRule="nonzero"
        mask="url(#Settings_svg__b)"
      />
    </g>
  </svg>
)

export default SvgSettings
