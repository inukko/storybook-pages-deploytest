import React from 'react'

const SvgStar = props => (
  <svg viewBox="0 0 20 19" fill="none" {...props}>
    <path
      d="M10 15.27L16.18 19l-1.64-7.03L20 7.24l-7.19-.61L10 0 7.19 6.63 0 7.24l5.46 4.73L3.82 19 10 15.27z"
      fill="#1CB569"
    />
  </svg>
)

export default SvgStar
