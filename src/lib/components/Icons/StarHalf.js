import React from 'react'

const SvgStarHalf = props => (
  <svg viewBox="0 0 18 17" {...props}>
    <defs>
      <path id="StarHalf_svg__a" d="M0 0h20v20H0z" />
    </defs>
    <g transform="translate(-.808 -1)" fill="none" fillRule="evenodd">
      <mask id="StarHalf_svg__b" fill="#fff">
        <use xlinkHref="#StarHalf_svg__a" />
      </mask>
      <path
        d="M18.333 7.7l-5.991-.517L10 1.667 7.658 7.192 1.667 7.7l4.55 3.942L4.85 17.5 10 14.392l5.15 3.108-1.358-5.858L18.333 7.7zM10 12.833v-7.75l1.425 3.367 3.65.317-2.767 2.4.834 3.566-3.142-1.9z"
        fill="#1CB569"
        fillRule="nonzero"
        mask="url(#StarHalf_svg__b)"
      />
    </g>
  </svg>
)

export default SvgStarHalf
