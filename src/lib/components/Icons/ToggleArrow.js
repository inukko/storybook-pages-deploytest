import React from 'react'

const SvgToggleArrow = props => (
  <svg viewBox="0 0 12 8" {...props}>
    <path
      d="M10.59.59L6 5.17 1.41.59 0 2l6 6 6-6z"
      fill="#1CB569"
      fillRule="nonzero"
      fillOpacity={0.8}
    />
  </svg>
)

export default SvgToggleArrow
