import React from 'react'

const SvgTwitter = props => (
  <svg viewBox="0 0 25 25" {...props}>
    <defs>
      <path id="Twitter_svg__a" d="M0 0h25v25H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <g>
        <mask id="Twitter_svg__b" fill="#fff">
          <use xlinkHref="#Twitter_svg__a" />
        </mask>
        <path
          d="M12.5 24.5c-6.627 0-12-5.373-12-12s5.373-12 12-12 12 5.373 12 12-5.373 12-12 12z"
          fill="#1CB569"
          fillRule="nonzero"
          mask="url(#Twitter_svg__b)"
        />
      </g>
      <path
        d="M20.013 8.92a5.919 5.919 0 0 1-1.703.466 2.975 2.975 0 0 0 1.304-1.64 5.939 5.939 0 0 1-1.884.719 2.967 2.967 0 0 0-5.055 2.706 8.421 8.421 0 0 1-6.115-3.1 2.966 2.966 0 0 0 .918 3.96 2.946 2.946 0 0 1-1.343-.37v.037a2.968 2.968 0 0 0 2.38 2.909 2.956 2.956 0 0 1-1.34.051 2.97 2.97 0 0 0 2.77 2.06 5.952 5.952 0 0 1-4.391 1.228A8.391 8.391 0 0 0 10.1 19.28c5.457 0 8.44-4.52 8.44-8.44 0-.129-.002-.257-.008-.384a6.016 6.016 0 0 0 1.48-1.536z"
        fill="#FFF"
        fillRule="nonzero"
      />
    </g>
  </svg>
)

export default SvgTwitter
