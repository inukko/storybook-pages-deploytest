import React from 'react'

const SvgVerified = props => (
  <svg viewBox="0 0 25 25" fill="none" {...props}>
    <mask
      id="Verified_svg__a"
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={25}
      height={25}
    >
      <path fill="#fff" d="M0 0h25v25H0z" />
    </mask>
    <g mask="url(#Verified_svg__a)">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M24.5 12.5c0 6.627-5.373 12-12 12s-12-5.373-12-12 5.373-12 12-12 12 5.373 12 12z"
        fill="#1CB569"
      />
    </g>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M5 6.273L12.364 3l7.363 3.273v4.909c0 4.54-3.142 8.787-7.363 9.818C8.142 19.97 5 15.723 5 11.182v-4.91zm2.455 6.545l3.272 3.273 6.546-6.546-1.154-1.161-5.392 5.391-2.119-2.11-1.153 1.153z"
      fill="#fff"
    />
  </svg>
)

export default SvgVerified
