import React from 'react'

const SvgWidgets = props => (
  <svg viewBox="0 0 25 25" {...props}>
    <defs>
      <path id="Widgets_svg__a" d="M0 0h25v25H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <mask id="Widgets_svg__b" fill="#fff">
        <use xlinkHref="#Widgets_svg__a" />
      </mask>
      <path
        d="M12.5 24.5c-6.627 0-12-5.373-12-12s5.373-12 12-12 12 5.373 12 12-5.373 12-12 12z"
        fill="#1CB569"
        fillRule="nonzero"
        mask="url(#Widgets_svg__b)"
      />
      <path
        d="M13.282 13.366v6.626h6.625v-6.626h-6.625zM5 19.992h6.625v-6.626H5v6.626zM5 5.085v6.625h6.625V5.085H5zM16.313 4l-4.688 4.68 4.688 4.686L21 8.68 16.313 4z"
        fill="#FFF"
        fillRule="nonzero"
      />
    </g>
  </svg>
)

export default SvgWidgets
