import React from 'react'
import styled from 'styled-components'
import themeGet from '@styled-system/theme-get'
import PropTypes from 'prop-types'

import { unit } from '../utils/unit'

import Checkmark from './Icons/Checkmark'
import ErrorIcon from './Icons/ErrorIcon'
import Icon from './Icon'
import LabelText from './LabelText'

const StyledContainer = styled.div`
  display: flex;
  width: 100%;
  margin: 0;
  padding: 0;
  align-items: center;
`

const StyledInputShape = styled.div`
  margin: 0.5rem;
  border-style: solid;
  border-width: 3px;
  border-color: ${props =>
    props.errors === false
      ? themeGet('colors.secondary')
      : props.errors
      ? themeGet('colors.danger')
      : 'transparent'};
  border-radius: ${themeGet('border.radius')};
  display: flex;
  padding: 1rem;
  flex-direction: column;
  width: ${props => unit(props.width || 20)};
  box-shadow: ${props => (props.errors ? themeGet('shadows.low') : 'none')};
  box-shadow: ${props =>
    props.active === true ? themeGet('shadows.high') : null};
`

function DisplayIcon(props) {
  if (props.errors === false) {
    return <Icon size={0.5} icon={Checkmark} alt="Checkmark" />
  } else if (props.errors) {
    return <Icon size={1} icon={ErrorIcon} alt="Error Icon" />
  }
  return null
}

function InputShape(props) {
  return (
    <StyledInputShape active={props.active} errors={props.errors} {...props}>
      <label>
        <LabelText label={props.label} errors={props.errors} />
        <StyledContainer>
          {props.children}
          <DisplayIcon errors={props.errors} />
        </StyledContainer>
      </label>
    </StyledInputShape>
  )
}

InputShape.propTypes = {
  active: PropTypes.bool,
  label: PropTypes.string,
  errors: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.func,
  ]),
}

export default InputShape
