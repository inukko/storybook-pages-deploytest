import React from 'react'
import styled from 'styled-components'
import themeGet from '@styled-system/theme-get'
import PropTypes from 'prop-types'

const StyledLabel = styled.span`
  color: ${props =>
    props.errors === false
      ? themeGet('colors.secondary')
      : props.errors
      ? themeGet('colors.danger')
      : themeGet('colors.idle')};
  font-size: 0.75rem;
  margin-bottom: 0.2rem;
  font-family: ${themeGet('fonts.title')};
`

function LabelText(props) {
  return (
    <StyledLabel errors={props.errors}>
      {props.label} {props.errors ? '(' + props.errors + ')' : null}
    </StyledLabel>
  )
}

LabelText.propTypes = {
  errors: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.func,
  ]),
  label: PropTypes.string,
}

export default LabelText
