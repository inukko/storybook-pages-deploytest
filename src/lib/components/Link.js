import styled from 'styled-components'

import linkColor from '../styles/linkColor'

const Link = styled.a`
  ${linkColor}
  padding: 0 0 0 0.3em;
`

export default Link
