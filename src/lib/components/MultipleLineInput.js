import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import FormWrapper from './FormWrapper'
import InputShape from './InputShape'

const Relative = styled.div`
  position: relative;
  width: 90%;
`

const DummyDiv = styled.div`
  position: relative;
  pointer-events: none;
  user-select: none;
  white-space: pre-wrap;
  word-wrap: break-word;
  width: 100%;
  visibility: hidden;
  z-index: -1;
`

const StyledTextareaAutosize = styled.textarea`
  position: absolute;
  color: inherit;
  background: none;
  border: none;
  resize: none;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
`

const StatelessMultipleLineInput = props => {
  const handleFocus = () => {
    props.setFormState({
      ...props,
      active: true,
    })
  }

  const handleBlur = () => {
    props.setFormState({
      ...props,
      active: false,
    })
  }

  const handleChange = event => {
    props.setFormState({
      ...props,
      value: event.target.value,
    })
  }
  return (
    <InputShape {...props}>
      <Relative>
        <DummyDiv>{props.value}</DummyDiv>
        <StyledTextareaAutosize
          {...props}
          minRows={props.minRows || 2}
          onChange={handleChange}
          onFocus={handleFocus}
          onBlur={handleBlur}
        />
      </Relative>
    </InputShape>
  )
}

const MultipleLineInput = props => (
  <FormWrapper
    {...props}
    render={name => <StatelessMultipleLineInput name={name} {...props} />}
  />
)

StatelessMultipleLineInput.propTypes = {
  active: PropTypes.bool,
  label: PropTypes.string,
  maxLength: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  minRows: PropTypes.number,
  name: PropTypes.string,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  size: PropTypes.number,
  value: PropTypes.string,
  errors: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.func,
  ]),
}

export default MultipleLineInput
