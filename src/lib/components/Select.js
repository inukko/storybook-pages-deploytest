import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import themeGet from '@styled-system/theme-get'

import ToggleArrow from '../assets/images/ToggleArrow.svg'

import FormWrapper from './FormWrapper'
import InputShape from './InputShape'

const StyledSelect = styled.select`
width: 100%;
appearance: none;
outline: none;
background: none;
border: none;
border-bottom: 3px solid ${themeGet('colors.idle')};
font-family: ${themeGet('fonts.title')};
font-size: 1rem;
color: ${themeGet('colors.secondary')};
background-repeat: no-repeat, repeat;
background-position: right 0.7em top 50%, 0 0;
background-size: 0.65em auto, 100%;
background-image: url('${ToggleArrow}');
option {
    background-color: ${themeGet('colors.background')};
    font-size: 1rem;
}
`

const Options = props => {
  return props.options.map((option, index) => {
    return (
      <option key={index} value={index} disabled={index === 0}>
        {option}
      </option>
    )
  })
}

const StatelessSelect = props => {
  const handleFocus = () => {
    props.setFormState({
      ...props,
      active: true,
    })
  }
  const handleChange = event => {
    props.setFormState({
      ...props,
      value: parseInt(event.target.value),
    })
  }
  const handleBlur = () => {
    props.setFormState({
      ...props,
      active: false,
    })
  }

  return (
    <InputShape {...props}>
      <StyledSelect
        onFocus={handleFocus}
        onChange={handleChange}
        onBlur={handleBlur}
        value={props.value || 0}
      >
        <Options options={props.options} />
      </StyledSelect>
    </InputShape>
  )
}

const Select = props => (
  <FormWrapper
    {...props}
    render={name => <StatelessSelect {...props} name={name} />}
  />
)

StatelessSelect.propTypes = {
  name: PropTypes.string.isRequired,
  setFormState: PropTypes.func,
  value: PropTypes.number,
  size: PropTypes.number,
  label: PropTypes.string,
  options: PropTypes.array,
  errors: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.func,
  ]),
}

export default Select
