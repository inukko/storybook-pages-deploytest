import styled from 'styled-components'
import themeGet from '@styled-system/theme-get'

const Subtitle = styled.p`
  font-family: ${themeGet('fonts.title')};
  font-weight: lighter;
`

export default Subtitle
