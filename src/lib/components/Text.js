import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'

import { unit, unitLength } from '../utils'

const fontSize = unitLength(0.3, 'rem')
const Text = styled.p`
  font-size: ${({ size }) => (size ? fontSize((size + 1) / 1.5 - 0.5) : '1em')};
  margin: ${unit(0.25)} auto;
  ${({ centered }) =>
    centered &&
    css`
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;
      word-wrap: anywhere;
    `}
  text-align: left;
  max-width: ${({ widthLimit }) => widthLimit || unit(12)};
  line-height: 1.55em;
`

Text.propTypes = {
  widthLimit: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  centered: PropTypes.bool,
}

export default Text
