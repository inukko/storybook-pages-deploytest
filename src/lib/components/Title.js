import styled from 'styled-components'
import themeGet from '@styled-system/theme-get'

const Title = styled.h1`
  font-size: 3em;
  font-family: ${themeGet('fonts.title')};
  font-weight: lighter;
`

export default Title
