import React from 'react'
import styled from 'styled-components'
import themeGet from '@styled-system/theme-get'
import PropTypes from 'prop-types'

import { unit } from '../utils'
import off from '../assets/images/InvertedOff.svg'
import on from '../assets/images/InvertedOn.svg'

import FormWrapper from './FormWrapper'

const animationTime = '0.4s'

// TODO: theme easing curve
const ToggleKnob = styled.img`
  display: flex;
  justify-content: center;
  align-items: center;
  height: ${({ size }) => unit(size)};
  width: ${({ size }) => unit(size)};
  font-size: ${({ size }) => unit(size / 1.3)};
  filter: drop-shadow(
    0
      ${({ size }) => {
        const processed = unit(size / 20)
        return `${processed} ${processed}`
      }}
      #0007
  );
  transform: ${({ checked }) => checked || 'rotate(-360deg)'};
  transition: transform ${animationTime};
`

const ToggleShape = styled.span`
  display: block;
  position: relative;
  margin: 0.5rem;
  display: flex;
  align-items: center;
  background-color: currentColor;
  transition: color 0.1s linear;
  user-select: none;
  cursor: pointer;
  height: ${({ size }) => unit(size)};
  width: ${({ size }) => unit(size * 2.87)};
  border-radius: ${({ size }) => unit(size)};
  border: ${({ size }) => unit(size / 15)} solid currentColor;
  color: ${({ checked }) =>
    themeGet(checked ? 'colors.secondary' : 'colors.danger')};
`

const CheckboxContainer = styled.span`
  display: inline-block;
  vertical-align: middle;
  transition: left ${animationTime} ${themeGet('transitions.main')};
  position: relative;
  left: ${({ checked, size }) =>
    checked ? `calc(100% - ${unit(size)})` : '0'};
`

const HiddenCheckbox = styled.input.attrs({ type: 'checkbox' })`
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`

// inspired by https://medium.com/@colebemis/building-a-checkbox-component-with-react-and-styled-components-8d3aa1d826dd
const Checkbox = ({ className, checked, size, label, ...props }) => (
  <CheckboxContainer className={className} size={size} checked={checked}>
    <HiddenCheckbox
      value={checked}
      checked={checked}
      aria-label={label}
      {...props}
    />
    <ToggleKnob src={checked ? on : off} checked={checked} size={size} />
  </CheckboxContainer>
)

const StatelessToggle = ({ value, size = 1, label, errors, ...props }) => {
  const handleChange = () => {
    props.setFormState({
      ...props,
      label: label,
      errors: errors,
      size: size,
      value: !value,
    })
  }
  return (
    <>
      <ToggleShape size={size} checked={value}>
        <Checkbox
          tabIndex={0}
          size={size}
          {...props}
          checked={value}
          onChange={handleChange}
        />
      </ToggleShape>
    </>
  )
}

const WrappedStatelessToggle = props => (
  <>
    <label>
      <StatelessToggle {...props} />
    </label>
  </>
)

const Toggle = props => (
  <FormWrapper
    {...props}
    render={name => <WrappedStatelessToggle name={name} {...props} />}
  />
)

StatelessToggle.propTypes = {
  id: PropTypes.string,
  value: PropTypes.bool,
  name: PropTypes.string,
  onClick: PropTypes.func,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  size: PropTypes.number,
}

export { Toggle, StatelessToggle }
