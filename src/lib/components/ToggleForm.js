import React from 'react'
import PropTypes from 'prop-types'

import FormWrapper from './FormWrapper'
import InputShape from './InputShape'
import { StatelessToggle } from './Toggle'

const StatelessToggleForm = props => (
  <InputShape {...props}>
    <StatelessToggle {...props} />
  </InputShape>
)

const ToggleForm = props => (
  <FormWrapper
    {...props}
    render={name => <StatelessToggleForm name={name} {...props} />}
  />
)

StatelessToggleForm.propTypes = {
  value: PropTypes.bool,
  active: PropTypes.bool,
  label: PropTypes.string,
  errors: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.func,
  ]),
}

export default ToggleForm
