import styled from 'styled-components'
import PropTypes from 'prop-types'
import themeGet from '@styled-system/theme-get'

const Bubble = styled.div`
  background-color: white;
  padding: 1em;
  ${({ doublebottom }) => doublebottom && 'padding-bottom: 3em;'}
  display: flex;
  align-items: center;
  align-content: stretch;
  justify-content: space-between;
  flex-direction: ${({ row }) => (row ? 'row' : 'column')};
  border-radius: ${themeGet('border.radius')};
  border-width: 0;
  border-style: solid;
  box-shadow: ${({ shadow }) => themeGet(`shadows.${shadow || 'medium'}`)};
  > * {
    flex: 1 auto;
  }
`

Bubble.propTypes = {
  doublebottom: PropTypes.bool,
  row: PropTypes.bool,
  shadow: PropTypes.string,
}

export default Bubble
