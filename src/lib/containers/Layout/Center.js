import styled, { css } from 'styled-components'
import themeGet from '@styled-system/theme-get'
import PropTypes from 'prop-types'

const fullSize = css`
  height: 100vh;
  width: 100vw;
`

const full = ({ full }) => full && fullSize
const spaced = ({ spaced }) => spaced && `> * { margin: 1em; }`
const bordered = ({ bordered }) =>
  bordered &&
  `border-width: 0.2em; border-style: solid; border-color: seashell; border-radius: ${themeGet(
    'border.radius'
  )};`
const background = ({ background }) =>
  background && `background-color: linen; color: teal`

const Center = styled.div`
  padding-top: 1em;
  display: flex;
  align-items: center;
  justify-content: center;
  flex: 1;
  flex-flow: ${({ column }) => (column ? 'column' : 'row')};

  ${full}
  ${spaced}
  ${bordered}
  ${background}
`

Center.propTypes = {
  full: PropTypes.bool,
  spaced: PropTypes.bool,
  bordered: PropTypes.bool,
  background: PropTypes.bool,
  column: PropTypes.bool,
}

export default Center
