import React from 'react'
import styled from 'styled-components'

import Center from './Center'

const FlexElement = styled.div`
  flex: ${({ flex }) => flex};
`

const Compartment = ({ custom = [16, 66], left, middle, right }) => {
  const [side, center] = custom
  return (
    <Center>
      <FlexElement flex={side}>{left}</FlexElement>
      <FlexElement flex={center}>{middle}</FlexElement>
      <FlexElement flex={side}>{right}</FlexElement>
    </Center>
  )
}

export default Compartment
