import styled from 'styled-components'

// FIXME take shadow from Lira's design -> assets/images/Linguala.svg
export default styled.div`
  filter: drop-shadow(-5px -5px 5px #000);
`
