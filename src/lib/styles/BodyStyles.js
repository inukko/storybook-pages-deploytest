import { createGlobalStyle } from 'styled-components'
import themeGet from '@styled-system/theme-get'

import normalize from './normalize'

const BodyStyles = createGlobalStyle`
  ${normalize}
  html {
    font-size: 20px;
  }

  body {
    margin: 0;
    height: 100%;
    color: ${themeGet('colors.idle')};
    background-color: ${themeGet('colors.background.agent')};
  }

  * {
    box-sizing: border-box;
  }

`

export default BodyStyles
