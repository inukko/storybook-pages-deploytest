import { createGlobalStyle } from 'styled-components'

import LingualaTitle from '../assets/fonts/Linguala-Title.woff2'
import LingualaText from '../assets/fonts/Linguala-Text.woff2'
import LingualaPasswordFont from '../assets/fonts/Linguala-Password.woff2'

const GlobalFonts = createGlobalStyle`
  @font-face {
    font-family: Linguala-Title;
    src: url(${LingualaTitle}) format('woff2');
    font-weight: normal;
    font-style: normal;
  }
  
  @font-face {
    font-family: Linguala-Text;
    src: url(${LingualaText}) format('woff2');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: Linguala-Password;
    src: url(${LingualaPasswordFont}) format('woff2');
    font-weight: normal;
    font-style: normal;
  }

  html {
    font-family: Linguala-Text;
  }
`

export default GlobalFonts
