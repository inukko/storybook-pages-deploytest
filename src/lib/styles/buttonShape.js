import { css } from 'styled-components'
import themeGet from '@styled-system/theme-get'

import { unit } from '../utils'

const buttonShape = css`
  border-style: unset;
  border-radius: ${themeGet('border.radius')};
  min-width: ${({ size, width }) => width || unit(size)};
  min-height: auto;
  padding: 5px;
`

export default buttonShape
