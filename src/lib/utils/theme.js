import theme from '../styles/theme'

export const colorType = ({ secondary, danger, disabled }) =>
  danger
    ? theme.colors.danger
    : disabled
    ? theme.colors.idle
    : secondary
    ? theme.colors.secondary
    : theme.colors.primary
