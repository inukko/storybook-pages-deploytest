import React from 'react'
import { storiesOf } from '@storybook/react'
import { boolean, number, text, withKnobs } from '@storybook/addon-knobs'
import { withA11y } from '@storybook/addon-a11y'

import { Button, Center } from '../../lib'

export default storiesOf('Atom/Buttons', module)
  .addDecorator(withKnobs, withA11y)
  .add('Button', () => (
    <Center column spaced>
      <Button
        size={number('Size in units', 2, {
          range: true,
          min: 1,
          max: 5,
          step: 0.5,
        })}
        noresize={boolean('noresize')}
        selected={boolean('selected')}
        danger={boolean('danger')}
      >
        {text('value', 'beauftragen')}
      </Button>
      <Button
        size={number('Size in units', 2, {
          range: true,
          min: 1,
          max: 5,
          step: 0.5,
        })}
        noresize={boolean('noresize')}
        disabled
        value="inaktiv"
      />
    </Center>
  ))
  .add('Button/states', () => (
    <Center column spaced>
      <Button>plain</Button>
      <Button selected>selected</Button>
      <Button danger>danger</Button>
      <Button disabled>disabled</Button>
    </Center>
  ))
  .add('Button/sizing', () => (
    <Center spaced bordered>
      <Center column spaced bordered background>
        width
        <Button width="400px"> width = 400px </Button>
        <Button width="200px"> width = 200px </Button>
        <Button width="12em"> width = 12em </Button>
        <Button width="20em"> width = 20em </Button>
        <Button width="20em">
          width = 20em with alot of text to show how the <i>width</i> prop
          influences the final width
        </Button>
      </Center>
      <Center column spaced bordered background>
        size
        <Button size={1}> size = 1 </Button>
        <Button size={2}> size = 2 </Button>
        <Button size={3}> size = 3 </Button>
        <Button size={4}> size = 4 </Button>
      </Center>
    </Center>
  ))
