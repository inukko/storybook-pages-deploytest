import React from 'react'
import { storiesOf } from '@storybook/react'
import styled from 'styled-components'
import { withA11y } from '@storybook/addon-a11y'

import {
  Form,
  MultipleLineInput,
  Select,
  Selector,
  SingleLineInput,
  Toggle,
  ToggleForm,
} from '../../lib'

const StyledContainer = styled.div`
  display: grid;
  align-items: start;
  grid-template-columns: auto auto;
`

const stories = storiesOf('Atom/Form Elements', module)
  .addDecorator(withA11y)
  .add('Multiple Line Input', () => (
    <React.Fragment>
      <Form
        initFormState={{
          multi1: {
            placeholder: 'normal',
            label: 'This is a label',
          },
          multi2: {
            placeholder: 'disabled',
            disabled: true,
            label: 'This textarea is disabled',
          },
          multi3: {
            placeholder: 'There is an error',
            errors: 'This text is not correct',
            label: 'Another input',
          },
          multi4: {
            placeholder: 'This field has a max length',
            errors: false,
            label: 'Correct input',
            maxLength: 5,
          },
        }}
      >
        <MultipleLineInput name="multi1" />
        <MultipleLineInput name="multi2" />
        <MultipleLineInput name="multi3" />
        <MultipleLineInput name="multi4" />
      </Form>
    </React.Fragment>
  ))
  .add('Toggle', () => (
    <Form
      initFormState={{
        toggle1: {
          label: 'Required',
          value: false,
          errors: 'Please check this box...',
        },
        toggle2: {
          label: 'Correct',
          value: true,
          errors: false,
        },
        toggle3: {
          value: false,
          label: 'Please tick me',
        },
        toggle4: {
          value: false,
          errors: 'This needs to be ticked',
          label: 'Tick me!!!',
        },
        toggle5: {
          value: true,
          errors: false,
          label: 'Well done!',
        },
      }}
    >
      <Toggle name="toggle1" />
      <Toggle name="toggle2" />
      <ToggleForm name="toggle3" />
      <ToggleForm name="toggle4" />
      <ToggleForm name="toggle5" />
    </Form>
  ))
  .add('Select', () => (
    <Form
      initFormState={{
        select1: {
          label: 'Please select an option:',
          placeholder: 'Amazing options',
          options: [
            'here are some options',
            'option1',
            'option2',
            'option3',
            'another option',
            'nooooo select me',
          ],
        },
        select2: {
          label: 'Please select an option:',
          placeholder: 'Amazing options',
          options: [
            'here are some options',
            'option1',
            'option2',
            'option3',
            'another option',
            'nooooo select me',
          ],
          value: 1,
          errors: "You didn't make a wise selection. Try again!!!",
        },
        select3: {
          label: 'Please select an option:',
          placeholder: 'Amazing options',
          options: [
            'here are some options',
            'option1',
            'option2',
            'option3',
            'another option',
            'nooooo select me',
          ],
          value: 5,
          errors: false,
        },
      }}
    >
      <Select name="select1"></Select>
      <Select name="select2"></Select>
      <Select name="select3"></Select>
    </Form>
  ))
  .add('Single Line Input', () => (
    <Form
      initFormState={{
        input1: {
          placeholder: 'Placeholder',
          label: 'Label',
        },
        input2: {
          placeholder: 'Danger',
          label: 'Danger',
          errors: 'Something went wrong here!!!',
        },
        input3: {
          placeholder: 'Correct',
          label: 'Correct',
          value: 'Correct',
          errors: false,
        },
      }}
    >
      <SingleLineInput name="input1" />
      <SingleLineInput name="input2" />
      <SingleLineInput name="input3" />
    </Form>
  ))
  .add('Single Line Input Password', () => (
    <Form
      initFormState={{
        input1: {
          placeholder: 'Placeholder',
          label: 'Label',
          type: 'password',
        },
        input2: {
          placeholder: 'Danger',
          label: 'Danger',
          errors: 'Something went wrong here!!!',
          type: 'password',
        },
        input3: {
          placeholder: 'Correct',
          label: 'Correct',
          value: 'Correct',
          errors: false,
          type: 'password',
        },
      }}
    >
      <SingleLineInput name="input1" />
      <SingleLineInput name="input2" />
      <SingleLineInput name="input3" />
    </Form>
  ))
  .add('Selector Single Choice', () => (
    <Form
      initFormState={{
        selector1: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
        },
        selector2: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
          errors: 'Something went wrong here!!!',
        },
        selector3: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
          required: true,
        },
        selector4: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
          optionsSelected: [3],
          required: true,
          errors: false,
        },
      }}
    >
      <StyledContainer>
        <Selector name="selector1" />
        <Selector name="selector2" />
        <Selector name="selector3" />
        <Selector name="selector4" />
      </StyledContainer>
    </Form>
  ))
  .add('Selector Single Choice with Background', () => (
    <Form
      initFormState={{
        selector1: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
          background: true,
        },
        selector2: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
          errors: 'Something went wrong here!!!',
          background: true,
        },
        selector3: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
          required: true,
          background: true,
        },
        selector4: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
          optionsSelected: [3],
          required: true,
          errors: false,
          background: true,
        },
      }}
    >
      <StyledContainer>
        <Selector name="selector1" />
        <Selector name="selector2" />
        <Selector name="selector3" />
        <Selector name="selector4" />
      </StyledContainer>
    </Form>
  ))
  .add('Selector multiple Choice', () => (
    <Form
      initFormState={{
        selector1: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
          multiple: true,
        },
        selector2: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
          errors: 'Something went wrong here!!!',
          multiple: true,
        },
        selector3: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
          required: true,
          multiple: true,
        },
        selector4: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
          optionsSelected: [3],
          required: true,
          errors: false,
          multiple: true,
        },
      }}
    >
      <StyledContainer>
        <Selector name="selector1" />
        <Selector name="selector2" />
        <Selector name="selector3" />
        <Selector name="selector4" />
      </StyledContainer>
    </Form>
  ))
  .add('Selector multiple Choice with Background', () => (
    <Form
      initFormState={{
        selector1: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
          background: true,
          multiple: true,
        },
        selector2: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
          errors: 'Something went wrong here!!!',
          background: true,
          multiple: true,
        },
        selector3: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
          required: true,
          background: true,
          multiple: true,
        },
        selector4: {
          placeholder: 'Select your favorite Chocolate',
          options: ['black', 'milk', 'white', 'ruby'],
          optionsSelected: [3],
          required: true,
          errors: false,
          background: true,
          multiple: true,
        },
      }}
    >
      <StyledContainer>
        <Selector name="selector1" />
        <Selector name="selector2" />
        <Selector name="selector3" />
        <Selector name="selector4" />
      </StyledContainer>
    </Form>
  ))

export default stories
