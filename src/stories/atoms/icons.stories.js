import React from 'react'
import styled from 'styled-components'
import { storiesOf } from '@storybook/react'
import { number, text, withKnobs } from '@storybook/addon-knobs'
import { withA11y } from '@storybook/addon-a11y'

import {
  Add,
  AdditionalServices,
  Agb,
  Agent,
  Agentur,
  AsteriksCircle,
  Auftragserteilung,
  CleanBrain,
  Communication,
  Currency,
  Dollar,
  Email,
  ErrorIcon,
  Euro,
  Expertise,
  Eye,
  Facebook,
  France,
  Germany,
  Google,
  GreatBritain,
  GroupTree,
  Handshake,
  Heart,
  Icon,
  Instagram,
  InvertedOff,
  InvertedOn,
  Italy,
  Kunde,
  LingualaDevelopersLogo,
  LingualaIcon,
  Logo,
  Market,
  MarketIcon,
  Membership,
  MessyBrain,
  PasswordLens,
  Percentage,
  Phone,
  Portfolio,
  Provision,
  Rating,
  Register,
  Request,
  RssFeed,
  Search,
  SelectedLens,
  Settings,
  Software,
  Star,
  StarEmpty,
  StarHalf,
  Team,
  Time,
  ToggleArrow,
  Translate,
  Twitter,
  UnselectedLens,
  Verified,
  Widgets,
  Worldwideweb,
  Yen,
} from '../../lib'

const IconsBackground = styled.div`
  display: flex;
  flex-wrap: wrap;
  background-color: beige;
  padding: 1em;
  border-radius: 4em;
`

const IconBackground = styled.div`
  margin: 1em;
  border: 1px dashed black;
`

const IconDescription = styled.div`
  margin: 0 auto;
  width: 100%;
  text-align: center;
`

export default storiesOf('Atom/Icons', module)
  .addDecorator(withKnobs, withA11y)
  .add('All Icons', () => (
    <IconsBackground>
      {Object.entries({
        Add,
        AdditionalServices,
        Agb,
        Agent,
        Agentur,
        AsteriksCircle,
        Auftragserteilung,
        CleanBrain,
        Communication,
        Currency,
        Dollar,
        Email,
        ErrorIcon,
        Euro,
        Expertise,
        Eye,
        Facebook,
        France,
        Germany,
        Google,
        GreatBritain,
        GroupTree,
        Handshake,
        Heart,
        Instagram,
        InvertedOff,
        InvertedOn,
        Italy,
        Kunde,
        LingualaIcon,
        LingualaDevelopersLogo,
        Logo,
        Market,
        MarketIcon,
        Membership,
        MessyBrain,
        PasswordLens,
        Percentage,
        Phone,
        Portfolio,
        Provision,
        Rating,
        Register,
        Request,
        RssFeed,
        Search,
        SelectedLens,
        Settings,
        Software,
        Star,
        StarEmpty,
        StarHalf,
        Team,
        Time,
        ToggleArrow,
        Translate,
        Twitter,
        UnselectedLens,
        Verified,
        Widgets,
        Worldwideweb,
        Yen,
      }).map(([name, icon], key) => (
        <div key={key}>
          <IconBackground>
            <Icon
              icon={icon}
              alt={name}
              colorPrimaryHover={true}
              colorSecondaryHover={true}
            />
          </IconBackground>
          <IconDescription>{name}</IconDescription>
        </div>
      ))}
    </IconsBackground>
  ))
  .add('Agent', () => (
    <Icon
      icon={Agent}
      alt="Agent"
      size={number('Size', 4)}
      colorPrimary={text('Color Primary', '')}
      colorPrimaryHover={text(
        'Color Primary Hover (accepts colors or true for default color)',
        'true'
      )}
    />
  ))
  .add('Agentur', () => (
    <Icon
      icon={Agentur}
      alt="Agentur"
      size={number('Size', 4)}
      colorPrimary={text('Color Primary', 'pink')}
      colorPrimaryHover={text(
        'Color Primary Hover (accepts colors or true for default color)',
        'yellow'
      )}
    />
  ))
  .add('InvertedOn', () => (
    <Icon
      icon={InvertedOn}
      alt={'Checked'}
      size={number('Size', 4)}
      style={{ background: 'black' }}
    />
  ))
  .add('InvertedOff', () => (
    <Icon
      icon={InvertedOff}
      alt={'Unchecked'}
      size={number('Size', 4)}
      style={{ background: 'black' }}
    />
  ))
  .add('Kunde', () => (
    <Icon
      icon={Kunde}
      alt="Kunde"
      size={number('Size', 4)}
      colorPrimary={text('Color Primary', '')}
      colorPrimaryHover={text(
        'Color Primary Hover (accepts colors or true for default color)',
        'true'
      )}
    />
  ))
  .add('Linguala Icon', () => (
    <Icon
      icon={LingualaIcon}
      alt="Linguala Logo"
      size={number('Size', 4)}
      colorPrimary={text('Color Primary', '')}
      colorPrimaryHover={text(
        'Color Primary Hover (accepts colors or true for default color)',
        'true'
      )}
      colorSecondary={text('Color Secondary', '')}
      colorSecondaryHover={text(
        'Color Secondary Hover (accepts colors or true for default color)',
        'true'
      )}
    />
  ))
  .add('Logo rotated', () => (
    <Icon
      icon={Logo}
      alt="Linguala icon (without text)"
      rotate={135}
      size={number('Size', 4)}
      colorPrimary={text('Color Primary', '')}
      colorPrimaryHover={text('Color Primary Hover (accepts colors)', 'yellow')}
      colorSecondary={text('Color Secondary', '')}
      colorSecondaryHover={text(
        'Color Secondary Hover (accepts colors)',
        'red'
      )}
    />
  ))
