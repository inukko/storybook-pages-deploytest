import React from 'react'
import { storiesOf } from '@storybook/react'
import { withA11y } from '@storybook/addon-a11y'

import { Bubble, Button, colors } from '../../lib'

const { green, blue, red } = colors

const stories = storiesOf('Atom/Layout', module)
  .addDecorator(withA11y)
  .add('Bubble', () => (
    <Bubble doublebottom style={{ backgroundColor: 'seashell' }}>
      Ciao
      <Bubble>
        Hola
        <Bubble>
          ?Como estàs?
          <Bubble>
            Mi amor
            <Bubble doublebottom>
              Bueno
              <Bubble
                style={{
                  backgroundColor: 'lightgreen',
                  color: 'white',
                }}
              >
                What?
                <Bubble row>
                  <div>
                    <Bubble style={{ backgroundColor: green }}>
                      Sorry, but I don&apos;t really speak spanish.
                    </Bubble>
                    <Bubble style={{ backgroundColor: blue }}>
                      Please help me and translate with
                    </Bubble>
                  </div>
                  <a href="https://linguala.com">
                    <Button
                      size={5}
                      style={{
                        color: 'yellow',
                        height: 'calc(100% - 2em)',
                      }}
                    >
                      <Bubble
                        style={{
                          backgroundColor: red,
                        }}
                      >
                        Linguala
                      </Bubble>
                    </Button>
                  </a>
                </Bubble>
              </Bubble>
            </Bubble>
          </Bubble>
        </Bubble>
      </Bubble>
    </Bubble>
  ))

export default stories
