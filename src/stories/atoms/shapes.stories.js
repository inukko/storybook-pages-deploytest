import React from 'react'
import styled from 'styled-components'
import { storiesOf } from '@storybook/react'
import { boolean, number, withKnobs } from '@storybook/addon-knobs'
import { withA11y } from '@storybook/addon-a11y'

import { Colorable, Lens, PinShape, Text } from '../../lib'

const Container = styled.div`
  display: flex;
  justify-content: space-evenly;
  min-width: 300px;
  width: 90vw;
`

storiesOf('Basic/Color', module)
  .addDecorator(withA11y)
  .add('Colorable', () => (
    <div>
      <Colorable full>primary</Colorable>
      <Colorable full secondary>
        secondary
      </Colorable>
      <Colorable full disabled>
        disabled
      </Colorable>
      <Colorable full danger>
        danger
      </Colorable>
    </div>
  ))

storiesOf('Atom/Shape', module).add('all', () => (
  <Container
    style={{
      alignItems: 'center',
    }}
  >
    <Lens>
      <Text>Lens</Text>
    </Lens>
    <PinShape>
      <Text>Pin</Text>
    </PinShape>
  </Container>
))

storiesOf('Atom/Shape', module)
  .addDecorator(withKnobs)
  .add('Lens Border', () => (
    <Lens
      size={number('Size in units', 10, { min: 2, max: 36, step: 2 })}
      rotation={number('Rotation in °', 0, { min: -360, max: 360, step: 15 })}
      danger={boolean('danger')}
      disabled={boolean('disabled')}
    />
  ))
  .add('Pin full & border variants', () => (
    <div>
      <Container>
        <PinShape
          full
          rotation={number('Rotation in °', 0, {
            min: -360,
            max: 360,
            step: 15,
            range: true,
          })}
          size={number('Size in units', 5, {
            range: true,
            min: 2,
            max: 14,
            step: 1,
          })}
          danger={boolean('danger')}
          disabled={boolean('disabled')}
        />
        <PinShape
          border
          rotation={number('Rotation in °', 0, {
            min: -360,
            max: 360,
            step: 15,
            range: true,
          })}
          size={number('Size in units', 5, {
            range: true,
            min: 2,
            max: 14,
            step: 1,
          })}
          danger={boolean('danger')}
          disabled={boolean('disabled')}
        />
      </Container>
      <Container
        style={{
          paddingTop: '20px',
        }}
      >
        <span>full</span>
        <span>border</span>
      </Container>
    </div>
  ))
