import React from 'react'
import { storiesOf } from '@storybook/react'
import { boolean, number, text, withKnobs } from '@storybook/addon-knobs'
import { withA11y } from '@storybook/addon-a11y'

import { Bubble, Link, Subtitle, Text, Title } from '../../lib'

storiesOf('Atom/Text', module)
  .addDecorator(withKnobs, withA11y)
  .add('Title', () => <Title>willkommen bei linguala</Title>)
  .add('SubTitle', () => (
    <Subtitle>
      der neutraler marktplatz für sprachservices - was möchten sie als nächstes
      machen?
    </Subtitle>
  ))
  .add('Text', () => (
    <Text
      widthLimit={text('widthLimit', '20em')}
      centered={boolean('centered', false)}
    >
      Dank Linguala werden Sprachbarrieren überwunden. Ob Einzelpersonen,
      Organisationen oder Unternehmen - Sie profitieren von Linguala. Kreiren
      Sie ein Profil um Favoriten zu speichern und browsen Sie den Linguala
      Marktplatz um Anbieter von Sprachservices zu finden.
    </Text>
  ))
  .add('Text/size', () => (
    <Bubble style={{ height: '85%' }}>
      <Text size={number('size', 2)} centered={boolean('centered', true)}>
        <p>
          Dank <Link href="//linguala.com">Linguala</Link> werden
          Sprachbarrieren überwunden. Ob Einzelpersonen, Organisationen oder
          Unternehmen - Sie profitieren von Linguala. Kreiren Sie ein Profil um
          Favoriten zu speichern und browsen Sie den Linguala
          <Link href="//linguala.lng.li/marketplace">Marktplatz</Link> um
          Anbieter von Sprachservices zu finden.
        </p>
        <p>
          Dieser Text benötigt einen flex-basierten Container zur Zentrierung.
        </p>
      </Text>
    </Bubble>
  ))
