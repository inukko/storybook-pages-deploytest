export default [
  'atoms/accordion.stories.js',
  'atoms/layout.stories.js',
  'atoms/shapes.stories.js',
  'atoms/text.stories.js',
  'atoms/button.stories.js',
  'atoms/icons.stories.js',
  'atoms/formElements.stories.js',
  'atoms/forms.stories.js',
  'molecules/shapes.stories.js',
]
