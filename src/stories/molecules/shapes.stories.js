import React from 'react'
import { storiesOf } from '@storybook/react'
import { boolean, number, withKnobs } from '@storybook/addon-knobs'
import { withA11y } from '@storybook/addon-a11y'

import { Currency, Icon, Lens, Logo, PinShape, Text } from '../../lib'

storiesOf('Molecules/Lens', module)
  .addDecorator(withKnobs, withA11y)
  .add('Logo + Lens Composition', () => (
    <Lens
      size={number('Size in units', 10, {
        range: true,
        min: 1,
        max: 42,
        step: 0.5,
      })}
      rotation={number('Rotation in °', 0, {
        range: true,
        min: -360,
        max: 360,
        step: 1,
      })}
      danger={boolean('danger')}
      disabled={boolean('disabled')}
    >
      <Logo />
    </Lens>
  ))
  .add('Icon + Lens Composition', () => (
    <Lens
      size={number('Size in units', 2, {
        range: true,
        min: 1,
        max: 12,
        step: 0.5,
      })}
      rotation={number('Rotation in °', 0, {
        range: true,
        min: -360,
        max: 360,
        step: 15,
      })}
      danger={boolean('danger')}
      disabled={boolean('disabled')}
    >
      <Icon icon={Currency} alt="Money" />
    </Lens>
  ))
  .add('Text + Lens Composition', () => (
    <Lens
      size={number('Size in units', 2, {
        range: true,
        min: 1,
        max: 12,
        step: 0.5,
      })}
      rotation={number('Rotation in °', 0, {
        range: true,
        min: -360,
        max: 360,
        step: 15,
      })}
      danger={boolean('danger')}
      disabled={boolean('disabled')}
    >
      <Text>Linguala</Text>
    </Lens>
  ))
storiesOf('Molecules/Pin', module)
  .addDecorator(withKnobs)
  .add('Logo + Pin Composition', () => (
    <PinShape
      size={number('Size in units', 2, {
        range: true,
        min: 1,
        max: 12,
        step: 0.5,
      })}
      rotation={number('Rotation in °', 0, {
        range: true,
        min: -360,
        max: 360,
        step: 15,
      })}
      danger={boolean('danger')}
      disabled={boolean('disabled')}
    >
      <Logo />
    </PinShape>
  ))
  .add('Icon + Pin Composition', () => (
    <PinShape
      size={number('Size in units', 2, {
        range: true,
        min: 1,
        max: 12,
        step: 0.5,
      })}
      rotation={number('Rotation in °', 0, {
        range: true,
        min: -360,
        max: 360,
        step: 15,
      })}
      danger={boolean('danger')}
      disabled={boolean('disabled')}
    >
      <Icon icon={Currency} alt="Money" />
    </PinShape>
  ))
  .add('Text + Pin Composition', () => (
    <PinShape
      size={number('Size in units', 2, {
        range: true,
        min: 1,
        max: 12,
        step: 0.5,
      })}
      rotation={number('Rotation in °', 0, {
        range: true,
        min: -360,
        max: 360,
        step: 15,
      })}
      danger={boolean('danger')}
      disabled={boolean('disabled')}
    >
      <Text>Linguala</Text>
    </PinShape>
  ))
