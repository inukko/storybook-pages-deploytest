import test from 'jest-t-assert'

import { centerPosition } from '../src/lib/utils/layout'

test('center with factor 1', t => {
  t.true(centerPosition(1) === '0%')
})

test('center with factor 2', t => {
  t.true(centerPosition(2) === '25%')
})

test('center with factor Infinity', t => {
  t.true(centerPosition(Infinity) === '50%')
})
