import test from 'jest-t-assert'

import { unit, unitLength } from '../src/lib/utils/index'

let utilFcts = { context: {} }

test.beforeEach(() => {
  utilFcts.context.calcWidth = unitLength()
  utilFcts.context.calcWidthParams = unitLength(10, 'px')
  utilFcts.context.relUnit = unitLength(1, 'rem')
})

test('initialize unitWidth', t => {
  const calcWidth = utilFcts.context.calcWidth
  t.true(calcWidth() === '20px')
  t.true(calcWidth(0) === '0px')
  t.true(calcWidth(1) === '20px')
  t.true(calcWidth(2) === '40px')
  t.true(calcWidth(3) === '60px')
})

test('initialize unitWidth with parameters', t => {
  const calcWidth = utilFcts.context.calcWidthParams
  t.true(calcWidth() === '10px')
  t.true(calcWidth(1) === '10px')
  t.true(calcWidth(2) === '20px')
  t.true(calcWidth(3) === '30px')
})

test('unit', t => {
  t.true(unit(1) === '20px')
  t.true(unit(2) === '40px')
  t.true(unit(4) === '80px')
})

test('customized unit', t => {
  const relUnit = utilFcts.context.relUnit
  t.true(relUnit(1) === '1rem')
  t.true(relUnit(2) === '2rem')
})
